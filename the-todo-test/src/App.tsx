import React from 'react';
import TaskList from './features/task/TaskList';
import './App.css';
import AddTask from './features/task/AddTask';

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <div>
          <AddTask />
          <TaskList />
        </div>
      </header>
    </div>
  );
}

export default App;
