import axios from 'axios';

export type Task = {
  id: number;
  userId: number;
  title: string;
  completed: boolean;
};

const TASKS_BASE_URL = 'https://jsonplaceholder.typicode.com/todos';

export async function fetchTasks(): Promise<Task[]> {
  const response = await axios.get(`${TASKS_BASE_URL}?completed=false`);
  return response.data;
}

export async function addTask(newTask: Task) {
  const response = await axios.post(TASKS_BASE_URL, newTask);
  // to get a random number above 300
  const newID = Math.floor(Math.random() * 101 + 300);
  const newTaskModified = { ...response.data, id: newID };
  return newTaskModified;
}

export async function deleteTask(task: Task) {
  const response = await axios.delete(`${TASKS_BASE_URL}/${task.id}`);
  if (response.status === 200) {
    return task;
  }
  return `${response.status}: ${response.statusText}`;
}
