import { Box } from '@mui/material';
import React, { useEffect } from 'react';

import { useAppSelector, useAppDispatch } from '../../app/hooks';
import TaskInfo from './TaskInfo';
import { fetchTasksAsync, selectTasks } from './taskSlice';

const TaskList = () => {
  const task = useAppSelector(selectTasks);
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(fetchTasksAsync());
  }, [dispatch]);

  const renderContent = () => {
    let content = null;
    if (task.loading) {
      content = <div>loading</div>;
    } else if (!task.loading && task.error) {
      content = <div>Error: {task.error}</div>;
    } else if (!task.loading && task.tasks.length > 0) {
      const reversedTasksArray = [...task.tasks].reverse();
      content = (
        <Box component='div' sx={{ paddingX: '20%' }}>
          {reversedTasksArray.map((task) => (
            <TaskInfo key={task.id} task={task} />
          ))}
        </Box>
      );
    }
    return content;
  };
  return <div>{renderContent()}</div>;
};

export default TaskList;
