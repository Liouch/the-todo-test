import { Box, Checkbox, FormControlLabel, FormGroup } from '@mui/material';
import React from 'react';
import { useAppDispatch } from '../../app/hooks';
import { Task } from './taskAPI';
import { deleteTaskAsync } from './taskSlice';
import { CheckOutlined, RadioButtonUnchecked } from '@mui/icons-material';

type Props = {
  task: Task;
};

const TaskInfo = ({ task }: Props) => {
  const dispatch = useAppDispatch();
  return (
    <Box
      component='span'
      sx={{
        borderBottom: '1px solid lightblue',
      }}
    >
      <FormGroup
        sx={{
          width: '100%',
          paddingX: '1.5rem',
          '&:hover': {
            backgroundColor: 'lightblue',
            color: 'whitesmoke',
          },
        }}
      >
        <FormControlLabel
          sx={{ width: '100%' }}
          control={
            <Checkbox
              icon={<RadioButtonUnchecked />}
              checkedIcon={<CheckOutlined />}
              id={task.id.toString()}
              onChange={() => dispatch(deleteTaskAsync(task))}
              tabIndex={0}
            />
          }
          label={task.title}
        />
      </FormGroup>
    </Box>
  );
};

export default TaskInfo;
