import React, { useState, useRef } from 'react';
import { Box, Button, TextField } from '@mui/material';
import { useAppDispatch } from '../../app/hooks';
import { Task } from './taskAPI';
import { addTaskAsync } from './taskSlice';

const AddTask = () => {
  const [inputValue, setInputValue] = useState('');
  const dispatch = useAppDispatch();
  const inputRef = useRef<HTMLInputElement>(null);

  const newTask: Task = {
    id: Math.floor(Math.random() * 101 + 300), // to get a randome number above 300
    userId: 1,
    completed: false,
    title: inputValue,
  };
  const onChangeHandlder = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    setInputValue(e.target.value);
  };

  const onClickHandler = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    if (!inputValue) {
      alert('Please write a task first');
    } else {
      dispatch(addTaskAsync(newTask));
      setInputValue('');
      console.log(inputRef);
      inputRef && inputRef.current?.focus();
    }
  };
  return (
    <Box
      component='form'
      noValidate
      autoComplete='off'
      sx={{
        borderBottom: '1px solid grey',
        display: 'flex',
        justifyContent: 'center',
        columnGap: '1.5rem',
        paddingY: '1.5rem',
      }}
    >
      <TextField
        id='outlined-basic'
        variant='outlined'
        placeholder='Add Task'
        value={inputValue}
        onChange={onChangeHandlder}
        size='small'
        inputRef={inputRef}
        tabIndex={0}
      />
      <Button variant='outlined' onClick={onClickHandler} tabIndex={0}>
        Add
      </Button>
    </Box>
  );
};
export default AddTask;
